package com.edwin.beans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean()
@SessionScoped
public class HelloWorldBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String msj;
	

	public String getMsj() {
		return msj;
	}

	public void setMsj(String msj) {
		this.msj = msj;
	}

	@Override
	public String toString() {
		return "HelloWorldBean [msj=" + msj + "]";
	}
	
}
